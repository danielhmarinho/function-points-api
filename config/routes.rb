Rails.application.routes.draw do
  resources :projects
  resources :organizations

  post 'authenticate', to: 'authentication#authenticate'
end
