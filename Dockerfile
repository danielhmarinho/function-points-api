FROM ruby:2.3.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /function-points
WORKDIR /function-points
ADD Gemfile /function-points/Gemfile
RUN bundle install
ADD . /function-points
